# Grabyo Poker

Created a poker application. The UI was already built, but the goal was to make the whole thing functional.

### Requirements

* A game can have between 2 and 6 players.
* You can add, remove, and edit a player
* The same card can not be present in two players hand
* A card needs to be "selected" in the card deck if it is added to a player's hand
* You are free to display the winner the way you want after clicking on the "Find the winner" button

### Packages used

https://www.npmjs.com/package/poker-hands

This package was used to determine which players had the best hand.

Each player object contained a card array:

![Screenshot 2019-07-29 at 10 28 29](https://user-images.githubusercontent.com/17644847/62037408-a2e72900-b1eb-11e9-9854-c708bb75e022.png)

Which was then converted into a string to check which hand was the winner -

![Screenshot 2019-07-29 at 10 29 46](https://user-images.githubusercontent.com/17644847/62037507-cf9b4080-b1eb-11e9-8d81-360f3c609ade.png)

## Technical details

### Develop

To start

```
yarn && yarn start
```

### Unit test

The tests are running using Jest and Enzyme

```
yarn test
yarn test -- --watch
```

### Functionality
You can add up to 6 players

![Screenshot 2019-07-28 at 16 24 12](https://user-images.githubusercontent.com/17644847/62009262-26097000-b155-11e9-8806-adbe36307fb7.png)

You can edit and save a players name

![Screenshot 2019-07-28 at 16 24 34](https://user-images.githubusercontent.com/17644847/62009261-2144bc00-b155-11e9-90b8-e5e761861ea2.png)

You can check who has the best hand - the winner will be displayed **BEAUTIFULLY!**

![Screenshot 2019-07-28 at 16 25 24](https://user-images.githubusercontent.com/17644847/62009245-fb1f1c00-b154-11e9-9497-59e5d0a6bd35.png)

Each player has a unique set of cards

Cards that appear in the players hand will be shown as ***selected*** in the deck above


### Errors
This game is between 2 and 6 players
Erros will appear if players are less than 2 or more than 6

![Screenshot 2019-07-28 at 16 31 59](https://user-images.githubusercontent.com/17644847/62009274-45080200-b155-11e9-840d-7e2fddbd9bf8.png)
![Screenshot 2019-07-28 at 16 32 17](https://user-images.githubusercontent.com/17644847/62009277-489b8900-b155-11e9-9cd4-20a172abc736.png)

If the deck of cards have run out when trying to add another player, an error will show

![Screenshot 2019-07-28 at 16 44 54](https://user-images.githubusercontent.com/17644847/62009434-112ddc00-b157-11e9-86db-ddb3403807ea.png)

### Future improvements

Given more time I would like to add:

- Reset / restart function to reset the game

- Deal function to redeal cards to each players

- Show a tally of how many times a winner has won

- If a player leaves - they can rejoin the game with the same name 

