import React, { Component } from 'react';
import poker from 'poker-hands';

import Layout from "./Layout";
import Deck from "./Deck";
import Player from "./Player";
import Button from "./Button";
import Celebration from "./Celebration";

import { suits, values } from "../utils";
import { Footer, AlertMessage } from "../Styles/Styled";

class App extends Component {
	selectedCardArray =[];

	state = {
		noCardError: false,
		deckOfCards: null,
		defaultPlayers: 2,
		edit: false,
		players: 2,
		playersArray: [],
		playersDeck: null,
		maxPlayerError: false,
		minPlayerError: false,
		showWinner: false,
		winnder: false,
	}

	componentWillMount() {
		this.getArrayOfCards();
	}

	setDefaultPlayers() {
		const defaultPlayersArr = this.defaultPlayers();
		this.setState({playersArray: defaultPlayersArr});
	}

	defaultPlayers() {
		const defaultPlayersArr = [];

		Array.from(Array(this.state.defaultPlayers)).map((player, index) => {
			const cards = this.cardsForEachPlayer();
	
			player = <Player
				name={`Player ${index + 1}`}
				key={index + 1}
				cards={cards} />;

			return defaultPlayersArr.push(player);
		});

		return defaultPlayersArr;
	}


	handleAddPlayer() {
	
		if (this.state.playersArray.length < 2) {

			this.setState({minPlayerError: true});

		} else if (this.state.playersArray.length >= 6) {

			this.setState({maxPlayerError: true});

		} else {
			const cards = this.cardsForEachPlayer();

			if (cards.length >= 5) {
				const newPlayer = <Player
					name={`Player ${this.state.players + 1}`}
					key={this.state.players + 1}
					cards={cards} />;
	
				this.setState({
					minPlayerError: false,
					maxPlayerError: false,
					players: this.state.players + 1,
					playersArray: [...this.state.playersArray, newPlayer],
					showWinner: false,
					winner: false,
				});
			} else { 
				this.setState({noCardError: true});
			}
			this.renderPlayers();
		}
	}

	handleDeletePlayer(ind) {

		if (this.state.playersArray.length > 2) {
			const newPlayersArray = this.renderPlayers();

			this.setState({
				minPlayerError: false,
				maxPlayerError: false,
				playersArray: newPlayersArray,
				players: this.state.players - 1,
			}, () => { this.removePlayerFunc(ind) });
		} else {
			return this.setState({ minPlayerError: true });
		}
	}


	removePlayerFunc(ind) {
		const updatedArry = [];

		this.state.playersArray.map((player) => {
			if (player.key != ind) {
				return updatedArry.push(player);
			}
		});

		this.setState({ playersArray: updatedArry }, () => {
			this.renderPlayers();
		});
	}

	handleFindWinner() {
		const cardsArray = [];

		this.state.playersArray.map((playersHand) => {
			const playerCards = playersHand.props.cards;
			let stringOfCards = "";

			playerCards.map(card => stringOfCards = stringOfCards + card.suit + card.value + " ");
			return cardsArray.push(stringOfCards);
		});

		const winner = poker.judgeWinner(cardsArray);
		this.setState({
			showWinner: true,
			winner: this.state.playersArray[winner].props.name,
		});
	}

	handleEditFunc(newName, index) {
		const newPlayerArr = JSON.parse(JSON.stringify(this.state.playersArray));
		newPlayerArr[index].props.name = newName;
		this.setState({playersArray: newPlayerArr}, () => console.log(this.state.playersArray));
	}

	renderPlayers() {
		const newPlayersArray = [];

		this.state.playersArray.map((player, ind) => {
			return newPlayersArray.push(<Player
				name={player.props.name}
				key={ind}
				index={ind}
				cards={player.props.cards}
				handleRemove={this.handleDeletePlayer.bind(this, ind)}
				handleEdit={this.handleEditFunc.bind(this)} />);
		});
		return newPlayersArray;
	}

	getArrayOfCards() {
		const cardArr = [];

		suits.map((suit) => {
			return values.map((value) => {
				const cardObject = {};
				cardObject.suit = suit;
				cardObject.value = value;
				return cardArr.push(cardObject);
			});
		});

		this.setState({deckOfCards: cardArr}, () => this.shuffleCards());
	}

	shuffleCards() {
		const copyOfDeck = JSON.parse(JSON.stringify(this.state.deckOfCards));
		const shuffledCards = copyOfDeck.sort(() => Math.random() - 0.5);

		return this.setState({playersDeck: shuffledCards}, () => this.setDefaultPlayers());
	}

	cardsForEachPlayer() {
		const playersCard = this.state.playersDeck.splice(0, 5);
		this.selectedCardArray = [...this.selectedCardArray, ...playersCard];
		return playersCard;
	}

	render() {
		const minPlayerError = <AlertMessage>Minimum of 2 players is required</AlertMessage>;
		const maxPlayerError = <AlertMessage>Maximum of 6 players is allowed</AlertMessage>;
		const noCardError = <AlertMessage>Sorry no more cards</AlertMessage>;

		return (
			<Layout>
				{this.state.showWinner && <Celebration winner={this.state.winner}/>}
				<section>
					<h1>Cards deck</h1>
					{this.selectedCardArray && this.state.deckOfCards && 
						<Deck
							cardDeck={this.state.deckOfCards}
							playersHand={this.selectedCardArray} />}
				</section>
				<section>
					<header>
						<h1>Players</h1>
					</header>
					<section>
						{this.renderPlayers()}
						{this.state.minPlayerError && minPlayerError}
						{this.state.maxPlayerError && maxPlayerError}
						{this.state.noCardError && noCardError}
					</section>
					<Footer>
						<Button
							icon="🙋‍♀️"
							onClick={this.handleAddPlayer.bind(this)}>Add new player</Button>
						<Button
							icon="🏆"
							onClick={this.handleFindWinner.bind(this)}>Find the winner</Button>
					</Footer>
				</section>
			</Layout>
		);
	}
}

export default App;