import React from "react";

import "../Styles/celebration.css";

const Celebration = ({winner}) => {
	return (
		<div className="pyro">
            <h1 className="winnder-text">Winner: {winner}!</h1>
            <div className="before"></div>
            <div className="after"></div>
        </div>
	)
};

export default Celebration;
