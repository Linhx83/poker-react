import React from "react";
import { Card, StyledDeck } from "../Styles/Styled";

const Deck = (props) => {
	const setSelectedCard = () => {
		props.cardDeck.map(card => {
			return props.playersHand.find(playerCard => {
				if (playerCard.suit === card.suit && playerCard.value === card.value) {
					card.selected = true;
					return card;
				}
			});
		});
		return props.cardDeck;
	}

	return (
		<StyledDeck>
			{setSelectedCard().map(card => {
				return <Card key={card.suit+card.value} suit={card.suit} value={card.value} selected={card.selected}>
					{card.value}
				</Card>
			})}
		</StyledDeck>
)};

export default Deck;
