import { mount } from "enzyme";

import React from "react";

import Deck from "./Deck";
import { Card } from "../Styles/Styled";

import { suits, values } from "../utils";

function getArrayOfCards() {
	const cardArr = [];

	suits.map((suit) => {
		return values.map((value) => {
			const cardObject = {};
			cardObject.suit = suit;
			cardObject.value = value;
			return cardArr.push(cardObject);
		});
	});
	return cardArr;
}

function shuffleCards() {
	const cardArray = getArrayOfCards();
	const copyOfDeck = JSON.parse(JSON.stringify(cardArray));
	const shuffledCards = copyOfDeck.sort(() => Math.random() - 0.5);

	return shuffledCards;
}

function cardsForEachPlayer() {
	const cardArray = shuffleCard();
	const playersCard = cardArray.splice(0, 5);
	return playersCard;
}

describe(`Card deck`, () => {
	test('renders the right amount of cards', () => {
		const cardArray = shuffleCard();
		const playersCard = cardsForEachPlayer();

		const deck = mount(<Deck cardDeck={cardArray} playersHand={playersCard} />)
		expect(deck.find(Card)).toHaveLength(52);
	});
});
