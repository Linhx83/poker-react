import React, { Component } from 'react';

import Button from "./Button";

import { Card, PlayerHand, Input } from "../Styles/Styled";

class Player extends Component {
	state = {
		edit: false,
		newValue: "",
	}

	saveOnEdit(index) {
		const newName = this.state.newValue ? this.state.newValue : this.props.name;
		this.setState({edit: false}, () => this.props.handleEdit(newName, index));
	}

	handleEditFunc() {
		this.setState({edit: !this.state.edit});
	}

	handleChange(event) {
		this.setState({newValue: event.target.value});
	}

	renderPlayersHand() {
		return this.props.cards.map((card, index) => {
			return <Card
				suit={card.suit}
				value={card.value}
				key={index}>{card.value}</Card>
		});
	}

	render() {
		return (
			<article>
				<p>
					{this.state.edit 
						? <Input type="text" value={this.state.newValue} onChange={this.handleChange.bind(this)}></Input>
						: this.props.name}
					<Button
						icon="✏️"
						onClick={() => this.saveOnEdit(this.props.index)}>Save</Button>
					<Button
						icon="✏️"
						onClick={this.handleEditFunc.bind(this)}>Edit</Button>
					<Button
						icon="🔥"
						onClick={this.props.handleRemove}>Remove</Button>
				</p>
				<PlayerHand>
					{this.renderPlayersHand()}
				</PlayerHand>
			</article>
		)
	}
};

export default Player;